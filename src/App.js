import React, { Component } from 'react';
import { ButtonToolbar, Button} from 'react-bootstrap';
import { Navbar,  NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import {
    BrowserRouter as Router,
    Route,
    Redirect,
    withRouter
} from "react-router-dom";


import Login  from './Auth'
import './App.css';
import Menu from "./Menu";



class App extends Component {
    render() {

        return <Router>
            <div id='menu'>

                <AuthButton />
                <Navbar>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <LinkContainer to="/protected">
                                <NavItem eventKey={1}>Home</NavItem>
                            </LinkContainer>
                        </Navbar.Brand>

                        </Navbar.Header>

                </Navbar>

                <Route path="/authenticate" component={Login}/>

                <PrivateRoute path="/protected" component={Menu}/>

            </div>

        </Router>

    }
}
export const OAuth = {
        isAuthenticated: false,
        authenticatedData: [],

      async authenticate(cb, provider_url) {

            try {
                var url = 'http://localhost:9090' + provider_url;

                const response = await fetch(url, {
                    method: 'GET',
                    mode: 'cors'
                });

                var json_response = await response.json();
                this.authenticatedData = await json_response;

                if (response.status.valueOf() === 200) {
                     this.isAuthenticated = await true;

                }
            } catch (e) {
                ;

            }
            /*
            this.isAuthenticated = true
            this.authenticatedData = {'uuid': '12345', 'user_email': 'richard.chomjak@gmail.com'}
            */

        },
        signout(cb) {
            this.isAuthenticated = false;
            this.authenticatedData = [];
        }
    };

export const BasketData = {
    basket_data: [],

     async get_basket(user_id)  {
        var url = await "http://localhost:9090/basket/add/" + user_id;

        const response = await fetch(url, {
            headers: {'Access-Control-Allow-Origin': '*'},
            method: 'GET',
            mode: 'cors'
        });

        var json_response = await response.json();

        //await this.setState({basket_data: json_response});

        BasketData.basket_data = await json_response;


    }

}

    const PrivateRoute = ({ component: Component, ...rest }) => (
        <Route
            {...rest}
            render={props =>
                OAuth.isAuthenticated ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: "/authenticate",
                            state: { from: props.location }
                        }}
                    />
                )
            }
        />
    );

    const AuthButton = withRouter(

        ({ history }) =>
            OAuth.isAuthenticated ? (
                <ButtonToolbar>
                <p>
                    {OAuth.authenticatedData.user_email}  {" "}
                    <Button bsStyle="danger"
                        onClick={() => {
                            OAuth.signout(() => history.push("/"));
                        }}
                    >
                         Sign out
                    </Button>
                </p>
                </ButtonToolbar>
            ) : (
                <p>You are not logged in.</p>
            )
    );

export default App;
