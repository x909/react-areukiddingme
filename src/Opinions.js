import React, {Component} from 'react';
import { Button} from 'react-bootstrap';
import { FormGroup, FormControl } from 'react-bootstrap';


class Opinion extends Component {

    constructor() {
        super();
        this.handleOpinionLoad = this.handleOpinionLoad.bind(this);
        this.handleDeleteSubmit = this.handleDeleteSubmit.bind(this);

        this.state = {
            data: []
        }

    }

    handleDeleteSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        var url = 'http://localhost:9090/opinions/del';

        fetch(url, {
            method: 'DELETE',
            body: data,
        });
    }

    componentDidMount() {
        this.handleOpinionLoad(this.props.match.params.id);

    }

    handleOpinionLoad = async function(id)  {
        var url = await "http://localhost:9090/opinions/product/" + id;

        const response = await fetch(url, {
            headers: {'Access-Control-Allow-Origin': '*'},
            method: 'GET',
            mode: 'cors'
        });

        await response.json().then((json) => this.setState({data: json}));


    }

   render() {

        let repr_data = this.state.data.map((opinion) =>
            <div key={opinion.id}>
                 <div className="opinion_sub">
                     <div>
                         {opinion.data}
                     </div>
                     <form onSubmit={this.handleDeleteSubmit}>
                         <FormGroup>
                         <FormControl id="id" name="id" type="hidden" value={opinion.id} />

                         <Button type="submit" bsStyle="danger">Delete Opinion</Button>
                         </FormGroup>
                     </form>

                 </div>
            </div>
        );

/*
       let data = this.props.state.products.map((prod) =>


           <div id={prod.id}>

               <div className="title"> {prod.name}</div>
               <div>product id: {prod.id}</div>
               <div>description: {prod.description}</div>
               <div>price {prod.price}</div>
               <form onSubmit={this.handleOpinionSubmit}>
                 opinions
               </form>

           </div>
       );
*/

        return (
            <div className="opinion">


                {repr_data}

            </div>
        )
    }
}

export default Opinion;