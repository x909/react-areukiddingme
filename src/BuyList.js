import React, {Component} from 'react';
import Opinion from './Opinions'
import {BasketData} from "./App";

import { Button} from 'react-bootstrap';
import { FormGroup, FormControl } from 'react-bootstrap';


import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'


class BuyList extends Component {

    constructor() {
        super();
        this.handleOpinionSubmit = this.handleOpinionSubmit.bind(this);
        this.handleAddToBasketProductSubmit = this.handleAddToBasketProductSubmit.bind(this);

    }


    handleOpinionSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        var url = 'http://localhost:9090/opinions/add';

        fetch(url, {
            method: 'POST',
            body: data,
        });
    }

    handleAddToBasketProductSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        var url = 'http://localhost:9090/basketprod/add';

        fetch(url, {
            method: 'POST',
            body: data,
        });
    }

    request_url = async function(id)  {
        var url = await "http://localhost:9090/opinions/product/" + id;

        const response = await fetch(url, {
            headers: {'Access-Control-Allow-Origin': '*'},
            method: 'GET',
            mode: 'cors'
        });
        var json_response = await response.json();
        return json_response;

    }




   render() {

       let data = this.props.state.products.map((prod) =>

           <div key={prod.id}>

               <div className="title"> {prod.name}</div>
               <div>product id: {prod.id}</div>
               <div>description: {prod.description}</div>
               <div>price: {prod.price}</div>
               <form onSubmit={this.handleOpinionSubmit}>
                 <FormGroup>
                     <FormControl
                        id="product_id" name="product_id" type="hidden" value={prod.id}
                     />
                     <FormControl
                    id="opinion" name="data" type="text" placeholder="Opinion"
                    />

                   <Button type="submit">Add opinon</Button>
                 </FormGroup>
               </form>

               <form onSubmit={this.handleAddToBasketProductSubmit}>
                   <FormGroup>
                    <FormControl
                        id="product_id" name="product_id" type="hidden" value={prod.id}  />
                    <FormControl
                        id="basket_id"  name="basket_id" type="hidden" value={BasketData.basket_data.id} />
                    <FormControl id="quantity" name="quantity" type="number" min="1" required placeholder="Quantity"/>

                   <Button type="submit">Add to basket</Button>
                   </FormGroup>
               </form>

               <Router>
               <div>
                        <Link to={`/opinion/${prod.id}`}>Opinions</Link>
                        <Route path='/opinion/:id' component={Opinion} />
               </div>
               </Router>
                <hr/>
           </div>
       );

        return (

            <div className="products">
                {data}


            </div>
        )
    }
}

export default BuyList;