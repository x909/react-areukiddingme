import React, {Component} from 'react';
import Opinion from './Opinions'

import { Button} from 'react-bootstrap';
import { FormGroup, FormControl } from 'react-bootstrap';

import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'


class ProductList extends Component {

    constructor() {
        super();
        this.handleOpinionSubmit = this.handleOpinionSubmit.bind(this);
        this.handleDelProductSubmit = this.handleDelProductSubmit.bind(this);

    }


    handleOpinionSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        var url = 'http://localhost:9090/opinions/add';

        fetch(url, {
            method: 'POST',
            body: data,
        });
    }

    handleDelProductSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        var url = 'http://localhost:9090/product/del';

        fetch(url, {
            method: 'DELETE',
            body: data,
        });
    }

    request_url = async function(id)  {
        var url = await "http://localhost:9090/opinions/product/" + id;

        const response = await fetch(url, {
            headers: {'Access-Control-Allow-Origin': '*'},
            method: 'GET',
            mode: 'cors'
        });
        var json_response = await response.json();
        return json_response;

    }

   render() {

       let data = this.props.state.products.map((prod) =>

           <div key={prod.id}>

               <div> {prod.name}</div>
               <div>product id: {prod.id}</div>
               <div>description: {prod.description}</div>
               <div>price {prod.price}</div>
               <form onSubmit={this.handleOpinionSubmit}>
                    <FormGroup>
                        <FormControl
                        id="product_id" name="product_id" type="hidden" value={prod.id}  />

                        <FormControl
                            id="opinion" name="data" type="text" placeholder="Opinion"/>


                   <Button type="submit">Add opinon</Button>
                    </FormGroup>
               </form>

               <form onSubmit={this.handleDelProductSubmit}>
                    <FormGroup>
                        <FormControl
                            id="product_id" name="id" type="hidden" value={prod.id}  />
                   <Button type="submit">Delete product</Button>
                    </FormGroup>
               </form>

               <Router>
               <div>
                        <Link to={`/opinion/${prod.id}`}>Opinions</Link>
                        <Route path='/opinion/:id' component={Opinion} />
               </div>
               </Router>
               <hr/>

           </div>
       );

        return (

            <div className="products">
                {data}


            </div>
        )
    }
}

export default ProductList;