import React, {Component} from 'react';
import ProductList from "./ProductsList";

class Products extends Component {

    constructor() {
        super();
        this.state = {
            products: [],
        };

    }

    request = async function()  {
        var url = await "http://localhost:9090/product/list";

        const response = await fetch(url, {
            headers: {'Access-Control-Allow-Origin': '*'},
            method: 'GET',
            mode: 'cors'
        });

        var json_response = await response.json();

        await this.setState({products: json_response});

    }

     componentDidMount() {

       this.request();

    }

    render() {
        return (
             <ProductList state={this.state}/>
        )
    }
}

export default Products;