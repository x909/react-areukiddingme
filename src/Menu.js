import React, { Component } from 'react';

import { Nav, Navbar, NavDropdown, MenuItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

import {
    BrowserRouter as Router,
    Route,

} from "react-router-dom";

import './App.css';
import Products from "./Products";
import ProductsForm from "./ProductForm";

import {BasketData, OAuth} from "./App";
import Buy from "./Buy";
import Basket from "./Basket";


class Menu extends Component {


    componentDidMount() {
        if (BasketData.basket_data.length === 0) {
            BasketData.get_basket(OAuth.authenticatedData.uuid)

        }
    }

    render() {
        return <Router>
            <div id='menu'>

                <Navbar>
                    <Nav bsStyle="tabs">


                        <NavDropdown eventKey={1} title="Products" id="1">

                            <LinkContainer to="/products">
                                <MenuItem eventKey={1.1}>List products</MenuItem>
                            </LinkContainer>


                            <LinkContainer to="/productsadd">
                                <MenuItem eventKey={1.2}>Create product</MenuItem>
                            </LinkContainer>

                        </NavDropdown>

                        <NavDropdown eventKey={2} title="Basket" id="2">

                            <LinkContainer to="/productsbuy">
                                <MenuItem eventKey={2.1}>Add Product</MenuItem>
                            </LinkContainer>


                            <LinkContainer to="/basket">
                                <MenuItem eventKey={2.2}>Basket</MenuItem>
                            </LinkContainer>

                        </NavDropdown>

                    </Nav>
                </Navbar>







                <Route path="/productsadd" component={ProductsForm}/>

                <Route path="/products" component={Products}/>

                <Route path="/productsbuy" component={Buy}/>
                <Route path="/basket" component={Basket}/>

            </div>



        </Router>

    }
}
export default Menu;