import React, {Component} from 'react';
import {BasketData} from "./App";

import BasketList from "./BasketList";

class Basket extends Component {

    constructor() {
        super();
        this.state = {
            in_basket_products: [],
        };


        this.request_data();

    }

    request_data = async function () {
        var url = await "http://localhost:9090/basketprod/get/" + BasketData.basket_data.id ;

        const response = await fetch(url, {
            headers: {'Access-Control-Allow-Origin': '*'},
            method: 'GET',
            mode: 'cors'
        });

        var json_response = await response.json();

        await this.setState({in_basket_products: json_response});


    }

    render() {
        return (
            <BasketList state={this.state}/>
        )
    }

}

export default Basket;