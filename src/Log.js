import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';
import './App.css';
import Products from './Products'
import ProductForm from './ProductForm'
import Auth from './Auth'



class App extends Component {
  render() {
        console.log(this)
    return <Router>
        <div id="menu">
            <ul>
                <li>
                    <Link to="/products">Products</Link>
                </li>
                <li>
                    <Link to="/productadd">Add Product</Link>
                </li>
                <li>
                    <Link to="/auth/google">Auth</Link>
                </li>


                <a href='http://localhost:9090/authenticate/google'> Google Auth</a>

            </ul>
        <Route path="/products" component={Products}/>
        <Route path="/productadd" component={ProductForm}/>
            <Route path="/auth/:id" component={Auth}/>

        </div>
    </Router>


  }
}

export default App;
