import React, {Component} from 'react';
import { Redirect } from 'react-router-dom';

import {Button} from 'react-bootstrap';

import {OAuth} from './App'

class LoginRedirect extends Component {

    constructor() {
        super();
        //Cheating :-))
        setTimeout(function() { this.setState({position: 1}); }.bind(this), 1500);
    }


   render() {


       console.log(OAuth)
       if (OAuth.isAuthenticated) {
           return <Redirect to="/" />;
       }

       return (
           <div>
               <form action="http://localhost:9090/authenticate/google">

                        <Button type="submit" bsStyle="primary">Google auth</Button>

               </form>
           </div>
       );
   }

}

export default LoginRedirect;