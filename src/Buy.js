import React, {Component} from 'react';
import BuyList from "./BuyList";

class Buy extends Component {

    constructor() {
        super();
        this.state = {
            products: [],
            basket_data:[]
        };


        this.request_data();

    }

    request_data = async function()  {
        var url = await "http://localhost:9090/product/list";

        const response = await fetch(url, {
            headers: {'Access-Control-Allow-Origin': '*'},
            method: 'GET',
            mode: 'cors'
        });

        var json_response = await response.json();

        await this.setState({products: json_response});

    }





    render() {
        return (
             <BuyList state={this.state}/>

        )
    }
}

export default Buy;