import React, {Component} from 'react';
import { Button} from 'react-bootstrap';
import { FormGroup, FormControl } from 'react-bootstrap';


class ProductsForm extends Component {

    constructor() {
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        var url = 'http://localhost:9090/product/add';

        fetch(url, {
            method: 'POST',
            body: data,
        });
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <FormGroup/>
                    <FormControl
                     id="name" name="name" type="text" placeholder="Product name" />
                    <FormControl
                          id="description" name="description" type="text" placeholder="Description" />

                    <FormControl
                        id="price" name="price" type="text" placeholder="Price" />

                <Button type="submit">Create product</Button>
                <FormGroup/>
            </form>
        );
    }

}


export default ProductsForm;