import React, {Component} from 'react';
import { Button} from 'react-bootstrap';
import { FormGroup, FormControl } from 'react-bootstrap';

class BasketList extends Component {

    constructor() {
        super();
        this.handleDelProductBasketSubmit = this.handleDelProductBasketSubmit.bind(this);

    }

    handleDelProductBasketSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);

        var url = 'http://localhost:9090/basketprod/del';

        fetch(url, {
            method: 'DELETE',
            body: data,
        });
    }




   render() {


       let total_price = this.props.state.in_basket_products.map((basket_prod) => [basket_prod.total_price])

       if (total_price.length === 0) {
           total_price[0] = 0;
       }
       let data = this.props.state.in_basket_products.map((basket_prod) =>



           <div key={basket_prod.id}>

               <div className="title"> {basket_prod.name}</div>
               <div>product id: {basket_prod.id}</div>
               <div>description: {basket_prod.description}</div>
               <div>price {basket_prod.price}</div>
               <div>Quantity {basket_prod.quantity}</div>



               <form onSubmit={this.handleDelProductBasketSubmit}>
                    <FormGroup>
                        <FormControl
                        id="product_id" name="product_id" type="hidden" value={basket_prod.product_id}/>
                   <FormControl id="basket_id" name="basket_id" type="hidden" value={basket_prod.basket_id}/>
                   <FormControl id="quantity" name="quantity" type="hidden" value={basket_prod.quantity}/>
                    </FormGroup>
                   <Button type="submit" bsStyle="warning">Remove from basket</Button>

               </form>

               <hr/>


           </div>
       );

        return (

            <div className="in_basket">
                <div name="basket_total_price"> Total price: {total_price[0]} </div>
                {data}


            </div>
        )
    }
}

export default BasketList;